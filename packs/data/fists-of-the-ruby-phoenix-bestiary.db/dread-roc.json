{
    "_id": "DW4UFHXzExWwvEuH",
    "data": {
        "abilities": {
            "cha": {
                "mod": 0
            },
            "con": {
                "mod": 7
            },
            "dex": {
                "mod": 3
            },
            "int": {
                "mod": -4
            },
            "str": {
                "mod": 9
            },
            "wis": {
                "mod": 2
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 36
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "",
                "max": 290,
                "temp": 0,
                "value": 290
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 26
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [
                    {
                        "type": "fly",
                        "value": "60"
                    }
                ],
                "value": "20"
            }
        },
        "details": {
            "alignment": {
                "value": "N"
            },
            "blurb": "",
            "creatureType": "Animal",
            "level": {
                "value": 15
            },
            "privateNotes": "",
            "publicNotes": "<p>Legendarily massive raptors capable of carrying off elephants as prey, rocs are typically about 30 feet long from beak to tail and have a wingspan of 80 feet or more. While their beaks are hooked to rip flesh from bone, their hunting strategy involves grabbing their prey in their powerful talons and then dropping it from great heights before feeding. This method creates a massive amount of carrion, which guarantees that rocs are followed by flocks of opportunistic scavengers, such as ravens and buzzards, who find it easy to steal bits of the larger birds' meals. Rocs, for the most part, don't mind these creatures, which sometimes get gobbled up along with the rest of the roc's food.</p>\n<p>Rocs usually nest among mountaintops and cliffs inaccessible to all but the bravest of terrestrial dwellers. They are long-range predators that hunt both land and sea in search for massive prey to sustain them and their young. Rocs are antisocial and lone hunters who compete with each other in fierce aerial battles to protect territory. But about once a decade, a mating couple pairs up to raise their chicks. Once the chicks are old enough to hunt on their own, the parents separate to once again engage in lone hunting.</p>\n<p>Particularly skilled druids or rangers might capture and train a roc to serve as a flying mount or hunting companion, though examples of such an incredible feat of domestication are few and far between. The easiest way to rear a roc is to do so from the moment it hatches, since the chick imprints on the first creature it sees. Acquiring a roc egg is by no means an easy feat, though, and is often a death sentence for the would-be egg-snatcher.</p>",
            "source": {
                "value": "Pathfinder #166: Despair on Danger Island"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 28
            },
            "reflex": {
                "saveDetail": "",
                "value": 25
            },
            "will": {
                "saveDetail": "",
                "value": 24
            }
        },
        "traits": {
            "attitude": {
                "value": "hostile"
            },
            "ci": [],
            "di": {
                "custom": "",
                "value": []
            },
            "dr": [],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": []
            },
            "rarity": {
                "value": "rare"
            },
            "senses": {
                "value": "low-light vision"
            },
            "size": {
                "value": "grg"
            },
            "traits": {
                "custom": "",
                "value": [
                    "animal"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/pf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "DOWxg5PQXXUG8Ddy",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "value": []
                },
                "bonus": {
                    "value": 30
                },
                "damageRolls": {
                    "luq868c26teqpyhmzl4n": {
                        "damage": "3d10+18",
                        "damageType": "piercing"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "reach-15"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Beak",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "Ka4LviiNUhy4FNzo",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "custom": "",
                    "value": [
                        "improved-grab"
                    ]
                },
                "bonus": {
                    "value": 30
                },
                "damageRolls": {
                    "kr4u1t9gej4iafu1k7rj": {
                        "damage": "3d8+18",
                        "damageType": "slashing"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "agile",
                        "reach-15"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Talon",
            "sort": 200000,
            "type": "melee"
        },
        {
            "_id": "sA9IvnRiBKdOegEF",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "custom": "",
                    "value": [
                        "improved-push"
                    ]
                },
                "bonus": {
                    "value": 30
                },
                "damageRolls": {
                    "wqd1x0z9oefxnzpmtt59": {
                        "damage": "2d6+15",
                        "damageType": "bludgeoning"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "agile",
                        "reach-30"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Wing",
            "sort": 300000,
            "type": "melee"
        },
        {
            "_id": "RUaJTvNfnNpOmBkz",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>@Localize[PF2E.NPC.Abilities.Glossary.LowLightVision]</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "low-light-vision",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.fFu2sZz4KB01fVRc"
                }
            },
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Low-Light Vision",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "xwHcXDjVTSiAD1kB",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "reaction"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p data-visibility=\"gm\"><strong>Trigger</strong> A creature moves from beyond the reach of the roc's wing to within the reach of the roc's wing.</p>\n<p><strong>Effect</strong> The roc makes a wing Strike against the triggering creature. If the roc Pushes the creature, it disrupts the triggering move action.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Reaction.webp",
            "name": "Wing Rebuff",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "4N1vw9fFoiAgxSn1",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 2
                },
                "description": {
                    "value": "<p>The roc Flies up to its Speed and makes two talon Strikes at any point during that movement. Each Strike must target a different creature. Each attack takes the normal multiple attack penalty.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/TwoActions.webp",
            "name": "Flying Strafe",
            "sort": 600000,
            "type": "action"
        },
        {
            "_id": "qJEKbHp6B8F9UKPL",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "free"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p><strong>Requirements</strong> The monster's last action was a success with a Strike that lists Improved Grab in its damage entry, or it has a creature grabbed using this action. <strong>Effect</strong> The monster automatically Grabs the target until the end of the monster's next turn. The creature is @Compendium[pf2e.conditionitems.Grabbed]{Grabbed} by whichever body part the monster attacked with, and that body part can't be used to Strike creatures until the grab is ended. Using Improved Grab extends the duration of the monster's Improved Grab until the end of its next turn for all creatures grabbed by it, but this costs an action. A grabbed creature can use the Escape action to get out of the grab, and the Grab ends for a grabbed creatures if the monster moves away from it.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/FreeAction.webp",
            "name": "Improved Grab",
            "sort": 700000,
            "type": "action"
        },
        {
            "_id": "P5b8rs3EYXgPccBp",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "free"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>@Localize[PF2E.NPC.Abilities.Glossary.Push]</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/FreeAction.webp",
            "name": "Improved Push",
            "sort": 800000,
            "type": "action"
        },
        {
            "_id": "fLIOuEGoDxmXYqNf",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A roc gains a +2 circumstance bonus to hit with its beak Strike if the target is grabbed or restrained in its talon.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Snack",
            "sort": 900000,
            "type": "action"
        },
        {
            "_id": "L1NeMv3ZDcttKLw2",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A roc can Fly at half Speed while it has a creature @Compendium[pf2e.conditionitems.Grabbed]{Grabbed} or @Compendium[pf2e.conditionitems.Restrained]{Restrained} in either or both of its talons, carrying that creature along with it.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Snatch",
            "sort": 1000000,
            "type": "action"
        },
        {
            "_id": "Eo6p25VuOxYy3RHK",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p>The dread roc screeches terrifyingly. Each creature in a 120-foot emanation must attempt a <span data-pf2-check=\"will\" data-pf2-traits=\"mental,fear,emotion\" data-pf2-label=\"Dreadful Screech DC\" data-pf2-dc=\"32\" data-pf2-show-dc=\"gm\">basic Will</span> save. Regardless of the result, creatures are temporarily immune for 1 minute.</p>\n<hr />\n<p><strong>Critical Success</strong> The creature is unaffected.</p>\n<p><strong>Success</strong> The creature is @Compendium[pf2e.conditionitems.Frightened]{Frightened 1}.</p>\n<p><strong>Failure</strong> The creature is @Compendium[pf2e.conditionitems.Frightened]{Frightened 2}.</p>\n<p><strong>Critical Failure</strong> The creature is fleeing for 1 round and @Compendium[pf2e.conditionitems.Frightened]{Frightened 3}.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "mental",
                        "auditory",
                        "emotion",
                        "fear"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/OneAction.webp",
            "name": "Dreadful Screech",
            "sort": 1100000,
            "type": "action"
        },
        {
            "_id": "gSOdjRxwJjv6KwzY",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 23
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Acrobatics",
            "sort": 1200000,
            "type": "lore"
        },
        {
            "_id": "VuFiACUoK3YO5qcp",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 31
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Athletics",
            "sort": 1300000,
            "type": "lore"
        }
    ],
    "name": "Dread Roc",
    "token": {
        "disposition": -1,
        "height": 4,
        "img": "systems/pf2e/icons/default-icons/npc.svg",
        "name": "Dread Roc",
        "width": 4
    },
    "type": "npc"
}
